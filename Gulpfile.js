var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('concat', function() {
	gulp.src('./client/**/*.js')
		.pipe(concat('dbapp.js'))
		.pipe(gulp.dest('./public/js'));
});

gulp.task('watch', function() {
	gulp.watch(['./client/**/*'], ['concat']);
});