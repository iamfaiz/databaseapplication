angular.module('dbapp', ['ui.router', 'ngHandsontable', 'toaster']);

angular.module('dbapp').config(['$stateProvider', '$locationProvider', '$urlRouterProvider',
	function($stateProvider, $locationProvider, $urlRouterProvider) {

		// $locationProvider.html5Mode(true);
		$stateProvider
			.state('home', {
				url: '/',
				template: '<h3>Welcome to the Database Application.</h3>'
			})
			.state('collections', {
				url: '/collections',
				templateUrl: '/views/collections.ng.html',
				controller: 'CollectionsListController'
			})
			.state('newCollection', {
				url: '/collection/new',
				templateUrl: '/views/newCollection.ng.html',
				controller: 'NewCollectionController'
			})
			.state('singleCollectionDesign', {
				url: '/collection/:collectionId/collection-design',
				templateUrl: '/views/single-collection-design.ng.html',
				controller: 'SingleCollectionDesignController'
			})
			.state('singleCollection', {
				url: '/collection/:collectionId',
				templateUrl: '/views/single-collection.ng.html',
				controller: 'SingleCollectionController'
			})
			;

		$urlRouterProvider.otherwise('/');

	}]);
angular.module('dbapp').factory('CollectionsService', ['$http', '$rootScope',
	function($http, $rootScope) {

		var CollectionsService = {};


		CollectionsService.saveCollection = function(collection) {
			if (typeof(collection) !== 'undefined') {
				var promise = $http({
					method: 'PUT',
					url: '/api/collections',
					headers: { 'Content-Type': 'application/json' },
					data: {
						collection: collection
					}
				});

				promise.then(function(res) {
					$rootScope.$emit('collection:updated:success', res);
				});

				return promise;
			}
		};

		CollectionsService.createCollection = function(collectionObject) {
			var promise = $http({
				method: 'POST',
				url: '/api/collections',
				headers: { 'Content-Type': 'application/json' },
				data: {
					collection: collectionObject
				}
			});

			promise.then(function(res) {
				$rootScope.$emit('collection:added:success', res);
			});

			return promise;
		};

		CollectionsService.getAll = function() {
			return $http({
				method: 'GET',
				url: '/api/collections'
			});
		};

		CollectionsService.getOne = function(id) {
			return $http({
				method: 'GET',
				url: '/api/collections/' + id
			});
		};

		CollectionsService.deleteCollection = function(id) {
			var promise = $http({
				method: 'DELETE',
				url: '/api/collections/' + id
			});

			promise.then(function(res) {
				$rootScope.$emit('collection:remove:success', res);
			});

			return promise;
		};

		return CollectionsService;

	}]);
angular.module('dbapp').controller('CollectionsController', ['$scope', '$state', '$stateParams', 'CollectionsService',
	function($scope, $state, $stateParams, CollectionsService) {

		CollectionsService.getOne($stateParams.collectionId).then(function(res) {
			$scope.collection = res.data;
		});

		$scope.newColumn = '';

		$scope.addRow = function() {
			$scope.collection.rows.push(generateNewRow($scope.collection.columns));
		};

		$scope.addColumn = function() {
			var columnName = $scope.newColumn;
			$('.modal-backdrop').fadeOut();
			$scope.collection.columns.push(columnName);
			for (var i = 0; i < $scope.collection.rows.length; i++) {
				if ( typeof ($scope.collection.rows[i][columnName]) === 'undefined' ) {
					$scope.collection.rows[i][columnName] = '';
				}
			}
			console.log($scope.collection.rows);

			CollectionsService.saveCollection($scope.collection);
			$state.go($state.current, { collectionId: $stateParams.collectionId }, {reload: true});
		};

		$scope.afterInit = function() {
			$scope.hotInstance = this;
		};

		$scope.afterChange = function() {
			CollectionsService.saveCollection($scope.collection);
		};

		function generateNewRow(columns) {
			var newRow = {};
			for(var i = 0; i < columns.length; i++) {
				newRow[columns[i]] = '';
			}
			return newRow;
		}

		$scope.$watch('collection', function(newVal, oldVal) {
			$scope.collection.updatedAt = Date.now();
			CollectionsService.saveCollection($scope.collection);
		}, true);

	}]);
angular.module('dbapp').controller('CollectionsListController', [ '$scope', 'CollectionsService', '$state', 'toaster',

	function($scope, CollectionsService, $state, toaster) {

		CollectionsService.getAll().then(function(res) {
			$scope.collections = res.data;
		});

		$scope.deleteCollection = function(collection) {
			CollectionsService.deleteCollection(collection._id);
			$state.go($state.current, {}, {reload: true});
		};

	}]);
angular.module('dbapp').controller('NewCollectionController', [ '$scope', 'CollectionsService', '$state', 
	function($scope, CollectionsService, $state) {

		$scope.newCollection = { name: 'New_Collection' };

		$scope.newCollection.columns = [
			{ name: '', datatype: 'text', uninque: true }
		];

		$scope.addColumn = function() {
			$scope.newCollection.columns.push( { name: '', datatype: 'text', uninque: false } );
		};

		$scope.save = function() {
			CollectionsService.createCollection($scope.newCollection).then(function(res) {
				$('.modal-backdrop').fadeOut();
				$state.go('collections');
			});			
		};

	}]);
angular.module('dbapp').controller('SidebarController', ['$scope', '$rootScope',
	function($scope, $rootScope) {


		$rootScope.selectedMenuItem = { label: 'Home', link: '#/' };
		
		$scope.menu = [
			{ label: 'Home', link: '#/', icon: 'fa fa-home' },
			{ label: 'Collections', link: '#/collections', icon: 'fa fa-database' },
			{ label: 'Sign Out', link: '/logout', icon: 'fa fa-sign-out' }
		];

		$scope.selectMenuItem = function (item) {
			$rootScope.selectedMenuItem = item;
		};

	}]);
angular.module('dbapp').controller('SingleCollectionController', [ '$scope', '$stateParams', 'CollectionsService',

	function ($scope, $stateParams, CollectionsService) {

		CollectionsService.getOne($stateParams.collectionId).then(function(res) {
			$scope.rawCollection = res.data;
			
			$scope.ngHandsontableData = { columns: [], rows: [] };
			// Generating columns for handsontable.
			for(var i = 0; i < $scope.rawCollection.columns.length; i++) {
				var currentColumnName = $scope.rawCollection.columns[i].name;
				$scope.ngHandsontableData.columns.push( currentColumnName );
			}

			// Modifiying our rows for handsontable.
			for (var j = 0; j < $scope.rawCollection.rows.length; j++) {
				for (var q = 0; q < $scope.ngHandsontableData.columns.length; q++) {
					var colIndexInTheLoop = $scope.ngHandsontableData.columns.indexOf($scope.ngHandsontableData.columns[q]);
					if ( typeof( $scope.rawCollection.rows[j][colIndexInTheLoop]) === 'undefined' ) {
						 $scope.rawCollection.rows[j][colIndexInTheLoop] = '';
					}
				}
			}
			
			$scope.addRow = function() {
				$scope.rawCollection.rows.push(generateRow($scope.rawCollection.columns));
			};

			function generateRow(columns) {
				var objectToGenerate = {};
				for (var i = 0; i < columns.length; i++) {
					var currentColumnIndex = columns.indexOf(columns[i]);
					objectToGenerate[currentColumnIndex] = '';
				}
				return objectToGenerate;
			}
		});

		$scope.collectionId = $stateParams.collectionId;

		$scope.$watch('rawCollection', function(newVal, oldVal) {
			if (typeof($scope.rawCollection) !== 'undefined') {
				CollectionsService.saveCollection(newVal);
			}
		}, true);

	}]);
angular.module('dbapp').controller('SingleCollectionDesignController', [ '$scope', '$stateParams', 'CollectionsService', 'toaster',
	function($scope, $stateParams, CollectionsService, toaster) {

		CollectionsService.getOne($stateParams.collectionId).then(function(res) {
			$scope.collection = res.data;
		});

		$scope.collectionId = $stateParams.collectionId;

		$scope.addColumn = function() {
			$scope.collection.columns.push({
				name: '',
				datatype: 'text',
				unique: false
			});
		};

		$scope.save = function() {
			CollectionsService.saveCollection($scope.collection).then(function(res) {
				$scope.collection = res.data;
				toaster.pop('success', 'Changes Saved!');
			});
		};

		$scope.deleteColumn = function(index) {
			// Update all the rows according to the deletion of the column.
			updateRowsAfterColumnDeletion($scope.collection, index);
			// Delete the column itself.
			$scope.collection.columns.splice(index, 1);
		};

		function updateRowsAfterColumnDeletion(collection, index) {
			var index = String(index);
			for (var i = 0; i < collection.rows.length; i++) {

				for (var key in collection.rows[i]) {
					
					if (key === index) {
						delete collection.rows[i][key];
					}
				}

			}
		}
		
	}]);
angular.module('dbapp').controller('ToasterController', [ '$rootScope' , 'toaster',
	function($rootScope, toaster) {


		$rootScope.$on('collection:updated:success', function(event, response) {

			console.log('Updated the collection!');

		});

		$rootScope.$on('collection:added:success', function(event, response) {

			toaster.pop('success', 'Success', 'Successfully added a new collection!');
			console.log('Added a new collection!');

		});

		$rootScope.$on('collection:remove:success', function(event, response) {
			toaster.pop('success', 'Success', 'Successfully deleted the collection');
			console.log('Deleted the collection!');
		});


	}]);