var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var session = require('express-session');
var mongoose = require('mongoose');
var fs = require('fs');
var ejs = require('ejs');
var flash = require('flash');
var passport = require('./auth.js');

// Connect to the mongodb
mongoose.connect('mongodb://localhost/dbapp');
// Require all the models in the models directory
var normalizedPath = path.join(__dirname, "models");
fs.readdirSync(normalizedPath).forEach(function(file) {
    require("./models/" + file);
});

var UsersController = require('./controllers/UsersController.js');


var routes = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator([]));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: 'asfjh843h8f3jhkj3h4' }));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.get('/login', function(req, res) {
    res.render('login');
});

app.get('/register', function(req, res) {
    res.render('register');
});
// Register a new user.
app.post('/register', UsersController.register);

app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/login');
});

app.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login'
}));

app.use(function (req, res, next) {
    if (req.user) {
        next();
    } else {
        res.redirect('/login');
    }
});

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
