var UserController = {};

var mongoose = require('mongoose');
var User = mongoose.model('User');

UserController.register = function(req, res) {
	// TODO: Validation.
	// TODO: Password Hashing.
	var user = new User();
	user.name = req.body.name;
	user.username = req.body.username;
	user.password = req.body.password;
	user.save(function(err) {
		if (err) console.log(err);
	});
	req.session['success'] = 'Successfully Registered!';
	res.redirect('/login');
};


module.exports = UserController;