var CollectionsController = {};

var mongoose = require('mongoose');
var Collection = mongoose.model('Collection');


CollectionsController.index = function(req, res) {
	Collection.find({ userId: req.user._id }, function(err, docs) {
		res.send(docs);
	});
};

CollectionsController.show = function(req, res) {
	Collection.findOne({ _id: req.params.id, userId: req.user._id }, function(err, doc) {
		if (err) console.log(err);

		res.send(doc);
	});
};

CollectionsController.store = function(req, res) {
	var newCollection = new Collection();
	newCollection.name = req.body.collection.name;
	newCollection.columns = req.body.collection.columns;
	newCollection.userId = req.user._id;

	newCollection.save(function(err, doc) {
		if (err) console.log(err);

		res.send(doc);
	});
};

CollectionsController.update = function(req, res) {
	Collection.findOneAndUpdate({ _id: req.body.collection._id, userId: req.user._id }, req.body.collection, {}, function(err, doc) {
		if (err) console.log(err);

		// CollectionsController.updateAllRowsAccordingToColumns(req.body.collection, res);
		Collection.findOne({ _id: req.body.collection._id, userId: req.user._id }, function(err, doc) {
			res.send(doc);
		});
	});
};

CollectionsController.delete = function(req, res) {
	Collection.remove({ _id: req.params.id, userId: req.user._id }, function(err) {
		if (err) console.log(err);

		res.end();
	});
};

CollectionsController.updateAllRowsAccordingToColumns = function(collection, res) {
	Collection.findOne({ _id: collection._id }, function(err, col) {
		if (err) console.log(err);

		for (var i = 0; i < col.rows.length; i++) {

			for (var j = 0; j < col.columns.length; j++) {
				if ( typeof(col.rows[i][col.columns[j].name]) === 'undefined' ) {
					col.rows[i][col.columns[j].name] = '';
				}
			}

		}

		col.save(function (err) {
			console.log(err)

			res.send(col);
		});
	});
};


module.exports = CollectionsController;