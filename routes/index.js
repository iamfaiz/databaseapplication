var express = require('express');

var router = express.Router();

var path = require('path');

var CollectionsController = require('../controllers/CollectionsController.js');

// Send the angularjs app from app.html file.
router.get('/', function(req, res) {
	res.sendFile(path.resolve('./views/html/app.html'));
});
// Show all the collections for the authenticated user.
router.get('/api/collections', CollectionsController.index);
// Show a specific collection that matches the ID.
router.get('/api/collections/:id', CollectionsController.show);
// Store a new collection in the db.
router.post('/api/collections', CollectionsController.store);
// Update the sent collection.
router.put('/api/collections', CollectionsController.update);
// Delete the sent collection.
router.delete('/api/collections/:id', CollectionsController.delete);

module.exports = router;
