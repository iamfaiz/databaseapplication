angular.module('dbapp', ['ui.router', 'ngHandsontable', 'toaster']);

angular.module('dbapp').config(['$stateProvider', '$locationProvider', '$urlRouterProvider',
	function($stateProvider, $locationProvider, $urlRouterProvider) {

		// $locationProvider.html5Mode(true);
		$stateProvider
			.state('home', {
				url: '/',
				template: '<h3>Welcome to the Database Application.</h3>'
			})
			.state('collections', {
				url: '/collections',
				templateUrl: '/views/collections.ng.html',
				controller: 'CollectionsListController'
			})
			.state('newCollection', {
				url: '/collection/new',
				templateUrl: '/views/newCollection.ng.html',
				controller: 'NewCollectionController'
			})
			.state('singleCollectionDesign', {
				url: '/collection/:collectionId/collection-design',
				templateUrl: '/views/single-collection-design.ng.html',
				controller: 'SingleCollectionDesignController'
			})
			.state('singleCollection', {
				url: '/collection/:collectionId',
				templateUrl: '/views/single-collection.ng.html',
				controller: 'SingleCollectionController'
			})
			;

		$urlRouterProvider.otherwise('/');

	}]);