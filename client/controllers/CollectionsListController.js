angular.module('dbapp').controller('CollectionsListController', [ '$scope', 'CollectionsService', '$state', 'toaster',

	function($scope, CollectionsService, $state, toaster) {

		CollectionsService.getAll().then(function(res) {
			$scope.collections = res.data;
		});

		$scope.deleteCollection = function(collection) {
			CollectionsService.deleteCollection(collection._id);
			$state.go($state.current, {}, {reload: true});
		};

	}]);