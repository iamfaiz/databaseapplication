angular.module('dbapp').controller('CollectionsController', ['$scope', '$state', '$stateParams', 'CollectionsService',
	function($scope, $state, $stateParams, CollectionsService) {

		CollectionsService.getOne($stateParams.collectionId).then(function(res) {
			$scope.collection = res.data;
		});

		$scope.newColumn = '';

		$scope.addRow = function() {
			$scope.collection.rows.push(generateNewRow($scope.collection.columns));
		};

		$scope.addColumn = function() {
			var columnName = $scope.newColumn;
			$('.modal-backdrop').fadeOut();
			$scope.collection.columns.push(columnName);
			for (var i = 0; i < $scope.collection.rows.length; i++) {
				if ( typeof ($scope.collection.rows[i][columnName]) === 'undefined' ) {
					$scope.collection.rows[i][columnName] = '';
				}
			}
			console.log($scope.collection.rows);

			CollectionsService.saveCollection($scope.collection);
			$state.go($state.current, { collectionId: $stateParams.collectionId }, {reload: true});
		};

		$scope.afterInit = function() {
			$scope.hotInstance = this;
		};

		$scope.afterChange = function() {
			CollectionsService.saveCollection($scope.collection);
		};

		function generateNewRow(columns) {
			var newRow = {};
			for(var i = 0; i < columns.length; i++) {
				newRow[columns[i]] = '';
			}
			return newRow;
		}

		$scope.$watch('collection', function(newVal, oldVal) {
			$scope.collection.updatedAt = Date.now();
			CollectionsService.saveCollection($scope.collection);
		}, true);

	}]);