angular.module('dbapp').controller('ToasterController', [ '$rootScope' , 'toaster',
	function($rootScope, toaster) {


		$rootScope.$on('collection:updated:success', function(event, response) {

			console.log('Updated the collection!');

		});

		$rootScope.$on('collection:added:success', function(event, response) {

			toaster.pop('success', 'Success', 'Successfully added a new collection!');
			console.log('Added a new collection!');

		});

		$rootScope.$on('collection:remove:success', function(event, response) {
			toaster.pop('success', 'Success', 'Successfully deleted the collection');
			console.log('Deleted the collection!');
		});


	}]);