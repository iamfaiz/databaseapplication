angular.module('dbapp').controller('SingleCollectionController', [ '$scope', '$stateParams', 'CollectionsService',

	function ($scope, $stateParams, CollectionsService) {

		CollectionsService.getOne($stateParams.collectionId).then(function(res) {
			$scope.rawCollection = res.data;
			
			$scope.ngHandsontableData = { columns: [], rows: [] };
			// Generating columns for handsontable.
			for(var i = 0; i < $scope.rawCollection.columns.length; i++) {
				var currentColumnName = $scope.rawCollection.columns[i].name;
				$scope.ngHandsontableData.columns.push( currentColumnName );
			}

			// Modifiying our rows for handsontable.
			for (var j = 0; j < $scope.rawCollection.rows.length; j++) {
				for (var q = 0; q < $scope.ngHandsontableData.columns.length; q++) {
					var colIndexInTheLoop = $scope.ngHandsontableData.columns.indexOf($scope.ngHandsontableData.columns[q]);
					if ( typeof( $scope.rawCollection.rows[j][colIndexInTheLoop]) === 'undefined' ) {
						 $scope.rawCollection.rows[j][colIndexInTheLoop] = '';
					}
				}
			}
			
			$scope.addRow = function() {
				$scope.rawCollection.rows.push(generateRow($scope.rawCollection.columns));
			};

			function generateRow(columns) {
				var objectToGenerate = {};
				for (var i = 0; i < columns.length; i++) {
					var currentColumnIndex = columns.indexOf(columns[i]);
					objectToGenerate[currentColumnIndex] = '';
				}
				return objectToGenerate;
			}
		});

		$scope.collectionId = $stateParams.collectionId;

		$scope.$watch('rawCollection', function(newVal, oldVal) {
			if (typeof($scope.rawCollection) !== 'undefined') {
				CollectionsService.saveCollection(newVal);
			}
		}, true);

	}]);