angular.module('dbapp').controller('SingleCollectionDesignController', [ '$scope', '$stateParams', 'CollectionsService', 'toaster',
	function($scope, $stateParams, CollectionsService, toaster) {

		CollectionsService.getOne($stateParams.collectionId).then(function(res) {
			$scope.collection = res.data;
		});

		$scope.collectionId = $stateParams.collectionId;

		$scope.addColumn = function() {
			$scope.collection.columns.push({
				name: '',
				datatype: 'text',
				unique: false
			});
		};

		$scope.save = function() {
			CollectionsService.saveCollection($scope.collection).then(function(res) {
				$scope.collection = res.data;
				toaster.pop('success', 'Changes Saved!');
			});
		};

		$scope.deleteColumn = function(index) {
			// Update all the rows according to the deletion of the column.
			updateRowsAfterColumnDeletion($scope.collection, index);
			// Delete the column itself.
			$scope.collection.columns.splice(index, 1);
		};

		function updateRowsAfterColumnDeletion(collection, index) {
			var index = String(index);
			for (var i = 0; i < collection.rows.length; i++) {

				for (var key in collection.rows[i]) {
					
					if (key === index) {
						delete collection.rows[i][key];
					}
				}

			}
		}
		
	}]);