angular.module('dbapp').controller('SidebarController', ['$scope', '$rootScope',
	function($scope, $rootScope) {


		$rootScope.selectedMenuItem = { label: 'Home', link: '#/' };
		
		$scope.menu = [
			{ label: 'Home', link: '#/', icon: 'fa fa-home' },
			{ label: 'Collections', link: '#/collections', icon: 'fa fa-database' },
			{ label: 'Sign Out', link: '/logout', icon: 'fa fa-sign-out' }
		];

		$scope.selectMenuItem = function (item) {
			$rootScope.selectedMenuItem = item;
		};

	}]);