angular.module('dbapp').controller('NewCollectionController', [ '$scope', 'CollectionsService', '$state', 
	function($scope, CollectionsService, $state) {

		$scope.newCollection = { name: 'New_Collection' };

		$scope.newCollection.columns = [
			{ name: '', datatype: 'text', uninque: true }
		];

		$scope.addColumn = function() {
			$scope.newCollection.columns.push( { name: '', datatype: 'text', uninque: false } );
		};

		$scope.save = function() {
			CollectionsService.createCollection($scope.newCollection).then(function(res) {
				$('.modal-backdrop').fadeOut();
				$state.go('collections');
			});			
		};

	}]);