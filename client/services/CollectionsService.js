angular.module('dbapp').factory('CollectionsService', ['$http', '$rootScope',
	function($http, $rootScope) {

		var CollectionsService = {};


		CollectionsService.saveCollection = function(collection) {
			if (typeof(collection) !== 'undefined') {
				var promise = $http({
					method: 'PUT',
					url: '/api/collections',
					headers: { 'Content-Type': 'application/json' },
					data: {
						collection: collection
					}
				});

				promise.then(function(res) {
					$rootScope.$emit('collection:updated:success', res);
				});

				return promise;
			}
		};

		CollectionsService.createCollection = function(collectionObject) {
			var promise = $http({
				method: 'POST',
				url: '/api/collections',
				headers: { 'Content-Type': 'application/json' },
				data: {
					collection: collectionObject
				}
			});

			promise.then(function(res) {
				$rootScope.$emit('collection:added:success', res);
			});

			return promise;
		};

		CollectionsService.getAll = function() {
			return $http({
				method: 'GET',
				url: '/api/collections'
			});
		};

		CollectionsService.getOne = function(id) {
			return $http({
				method: 'GET',
				url: '/api/collections/' + id
			});
		};

		CollectionsService.deleteCollection = function(id) {
			var promise = $http({
				method: 'DELETE',
				url: '/api/collections/' + id
			});

			promise.then(function(res) {
				$rootScope.$emit('collection:remove:success', res);
			});

			return promise;
		};

		return CollectionsService;

	}]);