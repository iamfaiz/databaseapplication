var passport 		= require('passport');
var	LocalStrategy 	= require('passport-local').Strategy;
var fs 				= require('fs');
var mongoose 		= require('mongoose');
var path 			= require('path');

var normalizedPath = path.join(__dirname, "models");
fs.readdirSync(normalizedPath).forEach(function(file) {
    require("./models/" + file);
});

var User = mongoose.model('User');


passport.use(new LocalStrategy(
	function(username, password, done) {

		User.findOne({ username: username, password: password }, function(err, user) {
			if (err) done(null, false, { message: 'Incorrect username or password!' });
			
			return done(null, user);
		});

	}
));

passport.serializeUser(function(user, done) {
	done(null, user);
});

passport.deserializeUser(function(user, done) {
	done(null, user);
});

module.exports = passport;
