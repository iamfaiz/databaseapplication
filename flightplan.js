var plan = require('flightplan');

// configuration
plan.target('production', [
  {
    host: '46.101.134.53',
    username: 'faiz',
    port: 22,
    agent: process.env.SSH_AUTH_SOCK
  },
]);

// run commands on localhost
plan.local(function(local) {
  // uncomment these if you need to run a build on your machine first
  // local.log('Run build');
  // local.exec('gulp build');
 
 local.log('Copy files to remote hosts');
  var filesToCopy = local.exec('git ls-files', {silent: true});
  // rsync files to all the destination's hosts
  local.transfer(filesToCopy, '~/www/DatabaseApplication2');

});

plan.remote(function(remote) {
  
  remote.log('Change directory to /www/databaseapplication');
  remote.sudo('cd /www/databaseapplication');

  remote.log('git pull the changes');
  remote.sudo('git pull origin master');

  remote.log('Install Dependencies');
  remote.sudo('npm install');

  remote.log('Restart the forever service');
  remote.sudo('forever restart ./bin/www');

});
