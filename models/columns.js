var mongoose = require('mongoose');

var ColumnsSchema = new mongoose.Schema({
	name: {
		type: String
	},
	datatype: {
		type: String
	},
	unique: {
		type: Boolean
	}
});

var model = mongoose.model('Column', ColumnsSchema);
module.exports = model;