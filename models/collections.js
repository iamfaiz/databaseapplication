var mongoose = require('mongoose');

var CollectionSchema = new mongoose.Schema({
	name: String,
	columns: {
		type: Array,
		default: [{ name: String, datatype: String, unique: Boolean }]
	},
	rows: {
		type: Array,
		default: []
	},
	userId: {
		type: String
	},
	createdAt: { type: Date, default: Date.now },
	updatedAt: { type: Date, default: Date.now }
});

var model = mongoose.model('Collection', CollectionSchema);
module.exports = model;