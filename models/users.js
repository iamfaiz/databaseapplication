var mongoose = require('mongoose');

var UsersSchema = new mongoose.Schema({
	username: {
		type: String,
		unique: true
	},
	password: {
		type: String
	},
	name: {
		type: String
	}
});

var model = mongoose.model('User', UsersSchema);
module.exports = model;